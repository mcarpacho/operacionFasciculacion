/******************************************************************************/
/*                                                                            */
/*   Furgalladas                                                              */
/*   Project: operacionFasciculacion
/*                                                                            */
/*   This code is released under MIT license                                  */
/*                                                                            */
/******************************************************************************/


/** \file

   operacionFasciculacion.ino 


   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> Source:   </td> <td> operacionFasciculacion.ino             </td></tr>
   <tr> <td> Revision: </td> <td> 1.1                               </td></tr>
   <tr> <td> Status:   </td> <td> ACCEPTED                          </td></tr>
   <tr> <td> Author:   </td> <td> Miguel Carpacho                   </td></tr>
   <tr> <td> Date:     </td> <td> 19-Decembrer-2020 22:25:56        </td></tr>
   </table>

   \n
   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> COMPONENT: </td> <td> operacionFasciculacion </td></tr>
   <tr> <td> TARGET:    </td> <td> Arduino           </td></tr>
   </table>
*/

/*MIT License

Copyright (c) [2020] [M.Carpacho]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef I_OPERACIONFASCICULACION_CPP
#define I_OPERACIONFASCICULACION_CPP

//Pin definitions
#define PIN_PROBE     2u
#define PIN_SHOCK     5u
#define PIN_BUZZER    6u
#define PIN_MOTOR     9u
#define PIN_LED      12u

//Configuration macros
#define TRIGGER_FIRST_INTERVAL       500
#define TRIGGER_SECOND_INTERVAL     1000
#define TRIGGER_THIRD_INTERVAL      1500
#define TRIGGER_FULL_TIME           2000

//PWM configuration
#define PWM_CONFIG_PERIOD             20
#define MOTOR_SPEED_HIGH              50
#define MOTOR_SPEED_LOW                0
#define BUZZER_SWITCH_VALUE           50

//function definitions
void triggerRoutine();

//Global variables
bool gb_interruptTriggerFlag = false;
unsigned long gul_interruptTimestamp;


void setup(){
  //Pin configuration
  pinMode(PIN_PROBE, INPUT);
  pinMode(PIN_SHOCK, OUTPUT);
  pinMode(PIN_BUZZER, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(PIN_MOTOR, OUTPUT);
  pinMode(PIN_LED, OUTPUT);
  //Initialization
  digitalWrite(PIN_SHOCK,LOW);
  digitalWrite(PIN_BUZZER,HIGH);
  digitalWrite(PIN_LED,LOW);
  digitalWrite(PIN_MOTOR,LOW);
  digitalWrite(LED_BUILTIN,LOW);
  //Attach Interrupt
  attachInterrupt(digitalPinToInterrupt(PIN_PROBE), triggerRoutine, FALLING);
  //Serial debug
  Serial.begin(9600);
  
}
void loop(){
  if (gb_interruptTriggerFlag){
    if (millis()<gul_interruptTimestamp+TRIGGER_FIRST_INTERVAL){
      digitalWrite(PIN_SHOCK,HIGH);//Activate shock
      digitalWrite(PIN_LED,HIGH);
      digitalWrite(LED_BUILTIN,HIGH);
      analogWrite(PIN_MOTOR,MOTOR_SPEED_HIGH);//Turn on motor
      analogWrite(PIN_BUZZER,PWM_CONFIG_PERIOD);//Turn on buzzer
      delay(BUZZER_SWITCH_VALUE);
      digitalWrite(LED_BUILTIN,LOW);
      digitalWrite(PIN_BUZZER,HIGH);//Turn off buzzer
      delay(BUZZER_SWITCH_VALUE);
      digitalWrite(LED_BUILTIN,HIGH);
      analogWrite(PIN_BUZZER,PWM_CONFIG_PERIOD);//Turn on buzzer
      delay(BUZZER_SWITCH_VALUE);
      digitalWrite(LED_BUILTIN,LOW);
      digitalWrite(PIN_BUZZER,HIGH);//Turn off buzzer
      delay(BUZZER_SWITCH_VALUE);
      digitalWrite(LED_BUILTIN,HIGH);
      analogWrite(PIN_BUZZER,PWM_CONFIG_PERIOD);//Turn on buzzer
      delay(BUZZER_SWITCH_VALUE);
      digitalWrite(LED_BUILTIN,LOW);
      digitalWrite(PIN_BUZZER,HIGH);//Turn off buzzer
      delay(BUZZER_SWITCH_VALUE);
      digitalWrite(PIN_SHOCK,LOW);//Deactivate shock

    }else if (millis()<gul_interruptTimestamp+TRIGGER_SECOND_INTERVAL){
      analogWrite(PIN_MOTOR,MOTOR_SPEED_LOW);//Turn off motor
    }else if (millis()<gul_interruptTimestamp+TRIGGER_THIRD_INTERVAL){
      
    }
    
    if (millis()>gul_interruptTimestamp+TRIGGER_FULL_TIME){//Be sure to disable all
      gb_interruptTriggerFlag=false;
      digitalWrite(PIN_SHOCK,LOW);
      digitalWrite(PIN_BUZZER,HIGH);//Turn off buzzer
      digitalWrite(PIN_LED,LOW);
    }
    
  }
  
}

void triggerRoutine(){
  if (!gb_interruptTriggerFlag){
    //Serial.println("tr");
    gb_interruptTriggerFlag = true;
    gul_interruptTimestamp = millis();
  }
}


#endif // I_OPERACIONFASCICULACION_CPP
